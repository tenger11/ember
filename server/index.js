/* eslint-env node */
'use strict';

// To use it create some files under `mocks/`
// e.g. `server/mocks/ember-hamsters.js`
//
// const bodyParser = require('body-parser');

// module.exports = function(app) {
//   // app.get('/ember-hamsters', function(req, res) {
//   //   res.send('hello');
//   // });
//   var jwt = require('jsonwebtoken');
//   app.use(bodyParser.json({ type: 'application/vnd.api+json'}));
//   app.use(bodyParser.json({ type: 'application/json'}));
//   var user = {
// 		email: 'test',
// 		password: '123'
// 	};
//   app.post('/api/token', function(req, res){
// 	if(req.body.email === user.email && req.body.password === user.password){
// 		var token;

// 		token = jwt.sign({email: user.email}, 'secretkey');

// 		res.send({
// 			token: token
// 		});
// 	} else{
// 		res.status(401).end();
// 	}
// });

// };

// module.exports = function(app) {
// 	// var express = require('express');
// 	// var tokenRouter = express.Router();

// 	app.post('/token', function(req, res){
// 		console.log(req.body);
// 		if (req.body.grant_type === 'password') {
// 	      if (req.body.username === 'letme' && req.body.password === 'in') {
// 	        res.status(200).send('{ "access_token": "secret token!"}');
// 	      } else {
// 	        res.status(400).send('{ "error": "invalid_grant" }');
// 	      }
// 	    } else {
// 			res.status(400).send('{ "error": "unsupported_grant_type"}');
// 		}
// 	});

// 	//app.use('/', tokenRouter);
// }

// module.exports = function(app) {
//   const globSync   = require('glob').sync;
//   const mocks      = globSync('./mocks/**/*.js', { cwd: __dirname }).map(require);
//   const proxies    = globSync('./proxies/**/*.js', { cwd: __dirname }).map(require);
//   const bodyParser = require('body-parser');
//   // Log proxy requests
//   const morgan = require('morgan');
//   app.use(morgan('dev'));
//   app.use(bodyParser.json({ type: 'application/vnd.api+json'}));

//   //mocks.forEach(route => route(app));
//   proxies.forEach(route => route(app));
// };
module.exports = function(app) {
  var globSync = require('glob').sync;
  var bodyParser = require('body-parser');
  //var cors = require('cors');
  var mocks = globSync('./mocks/**/*.js', { cwd: __dirname }).map(require);
  var proxies = globSync('./proxies/**/*.js', { cwd: __dirname }).map(require);

  app.use(bodyParser.json({ type: 'application/*+json' }));
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  // Log proxy requests
  var morgan = require('morgan');
  app.use(morgan('dev'));

  // enable *all* CORS requests
  //app.use(cors());

  // mocks.forEach(function(route) { route(app); });
  // proxies.forEach(function(route) { route(app); });
};
