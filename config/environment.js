/* eslint-env node */
'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'emberapp',
    environment: environment,
    contentSecurityPolicy: {'connect-src': "'self' wss://*.firebaseio.com"},
    firebase: {
      apiKey: 'AIzaSyA_ruPwQyIjqKu3kvCmWfXK0wi1OCWc_CM',
      authDomain: 'myemberproject.firebaseapp.com',
      databaseURL: 'https://myemberproject.firebaseio.com/',
      storageBucket: 'myemberproject.appspot.com',
    },
    torii: {
      sessionServiceName: 'session'
    },
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  //  ENV['ember-simple-auth'] = {
  //   authenticationRoute: 'login',
  //   routeAfterAuthentication: 'protected',
  //   authorizer: 'authorizer:token'
  // };
  // ENV['ember-simple-auth-token'] = {
  //   identificationField: 'email',
  //   serverTokenEndpoint: '/api/token',
  //   refreshAccessTokens: true,
  //   refreshTokenPropertyName: 'refresh_token',
  //   authorizationPrefix: 'Bearer ',
  //   authorizationHeaderName: 'Authorization',
  //   headers: {},
  //   refreshLeeway: 300 // Refresh the token 5 minutes (300s) before it expires.
  // };


  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
