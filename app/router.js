import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.resource('tasks', function() {
    this.route('new');
    this.route('edit', {path: '/edit/:task_id'});
  });
  this.resource('branches', function() {
    this.route('newbranch');
    this.route('editbranch', {path: '/editbranch/:branch_id'});
  });
  this.route('login');
  this.route('protected');
  this.route('signup');
  this.route('private', { path: ':user_id' });
});

export default Router;
