import Component from '@ember/component';

export default Component.extend({
	actions:{
		submit: function(){
			this.sendAction('action', this.get('credentials'));
		}
	}
});
