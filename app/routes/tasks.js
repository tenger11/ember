import Route from '@ember/routing/route';
export default Route.extend({
	model: function(){
		return this.store.findAll('task');
	},
	beforeModel: function(){
		if(!this.get('session.isAuthenticated')){
			this.transitionTo('login');
		}
	}
});
