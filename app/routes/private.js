import Route from '@ember/routing/route';

export default Route.extend({
	beforeModel: function(){
		if(!this.get('session.isAuthenticated')){
			this.transitionTo('login');
		}
	},
	model: function(params){
	return this.store.find('user', params.user_id);
	}
});
