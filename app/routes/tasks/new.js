import Route from '@ember/routing/route';
import DS from 'ember-data';

export default Route.extend({
	setupContoller: function(controller) {
		controller.set('errors', DS.Errors.create());
	}
});
