import Route from '@ember/routing/route';
import Ember from 'ember';
import DS from 'ember-data';

export default Route.extend({
	model: function(params){
		return Ember.RSVP.hash ({
			person: this.store.findAll('task'),
			branch: this.store.find('branch', params.branch_id)
		})
		//return this.store.findAll('task');
	},
	setupContoller: function(controller) {
		controller.set('errors', DS.Errors.create());
	}
});
