import Route from '@ember/routing/route';
import Ember from 'ember';

export default Route.extend({
	model: function(){
		return Ember.Object.create({ identification: '', password: ''});
	},
	setupController: function(controller, model){
		controller.set('credentials', model);
		// controller.set('errorMessage', DS.Errors.create());
	},
	//session: Ember.inject.service(),
	// actions: {
	// 	authenticate: function (credentials) {
	// 		// var authenticator = 'authenticator:oauth2';
	// 		// var _this = this;
	// 		// this.get('session').authenticate(authenticator, credentials.identification, credentials.password).catch((reason) =>{
	// 		// 	_this.controller.set('errorMessage', reason.error);
	// 		// });
	// 		var email = credentials.identification;
	// 		var password = credentials.password;
	// 		// this.get('session').authenticate('authenticator:firebase', {
	// 		// 	'provider': 'password',
	// 		//    'email': email,
	// 		//    'password': password
	// 		// }).then(function() {
	// 		//    this.transitionTo('protected');
	// 		// }.bind(this));
	// 		let controller = this;
	// 		this.get('session').open('firebase', {
	// 		provider: 'password',
	// 		email: email || '',
	// 		password: password || '',
	// 		}).then(() => {
	// 		controller.set('email', null);
	// 		controller.set('password', null);
	// 		controller.transitionToRoute('protected');
	// 		}, (error) => {
	// 		console.log(error);
	// 		});
	// 		}
	// }
});
