import DS from 'ember-data';

export default DS.Model.extend({
	title: DS.attr('string'),
	taskid: DS.attr('string'),
	description: DS.attr('string'),
	cost: DS.attr('number'),
	end: DS.attr('date')
});
