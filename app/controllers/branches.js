import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
			deleteBranch: function(id){
			if (confirm("Are you sure!") == true) {
				this.store.findRecord('branch', id).then(function(branch){
				branch.deleteRecord();
				branch.save();
				})
			}
		}
	}
});
