import Controller from '@ember/controller';
import DS from 'ember-data';
import Validate from 'npm:validate.io';

export default Controller.extend({
	taskid: null,
	actions: {	
		selectTask(taskid) {
			//alert(taskid);
			var id = this.get('model.branch.id')
			this.set('taskid', taskid);
			this.set('bid', id);
		},
		editBranch: function(id){
			if(this.validate()){
			var self = this;
			var title = this.get('model.branch.title');
			var description = this.get('model.branch.description');
			var end = this.get('model.branch.end');
			var cost = this.get('model.branch.cost');
			var taskid = this.get('taskid');
			var bid = this.get('bid');
			this.store.findRecord('branch', id).then(function(task){
				task.set('title', title);
				task.set('description', description);
				task.set('end', new Date(end));
				task.set('cost', cost);
				if(taskid && bid && bid === id){
					task.set('taskid', taskid);
				}
				task.save();
				self.transitionToRoute('branches');
			});
			}
		}
	},
	validate: function() {
		this.set('errors', DS.Errors.create());
		if (!Validate.isUndefinedOrNull(this.get('taskid')) && Validate.isEmpty(this.get('taskid'))){
			this.get('errors').add('taskid', "Can't be empty");
		}
		if(Validate.isUndefinedOrNull(this.get('model.branch.title')) || Validate.isEmpty(this.get('model.branch.title'))){
			this.get('errors').add('title', "Can't be empty");
		}
		if(Validate.isUndefinedOrNull(this.get('model.branch.end')) || (Validate.isEmpty(this.get('model.branch.end')) && Validate.isAbsoluteTime(this.get('model.branch.end')))){
			this.get('errors').add('end', "Can't be empty");
		}
		return this.get('errors.isEmpty');
	}	
});
