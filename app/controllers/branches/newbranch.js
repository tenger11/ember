import Controller from '@ember/controller';
import DS from 'ember-data';
import Validate from 'npm:validate.io';

export default Controller.extend({
	actions: {
		selectTask(taskid) {
			this.set('taskid', taskid);
		},
		addBranch: function(){
			if(this.validate()){
			var title = this.get('title');
			var description = this.get('description');
			var end = this.get('end');
			var cost = this.get('cost');
			var taskid = this.get('taskid');
			var newBranch = this.store.createRecord('branch',{
				title: title,
				description: description,
				end: new Date(end),
				cost: cost,
				taskid: taskid
			});

			newBranch.save();

			this.setProperties({
				title: '',
				description: '',
				end: '',
				cost: '',
				taskid: ''
			});
			}
		}
	},
	validate: function() {
		this.set('errors', DS.Errors.create());
		if(Validate.isUndefinedOrNull(this.get('taskid')) || Validate.isEmpty(this.get('taskid'))){
			this.get('errors').add('taskid', "Can't be empty");
		}
		if(Validate.isUndefinedOrNull(this.get('title')) || Validate.isEmpty(this.get('title'))){
			this.get('errors').add('title', "Can't be empty");
		}
		if(Validate.isUndefinedOrNull(this.get('end')) || Validate.isEmpty(this.get('end'))){
			this.get('errors').add('end', "Can't be empty");
		}
		return this.get('errors.isEmpty');
	}
});
