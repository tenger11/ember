import Controller from '@ember/controller';
import DS from 'ember-data';
import Validate from 'npm:validate.io';

export default Controller.extend({
	actions: {
		addTask: function(){
			if(this.validate()){
			var title = this.get('title');
			var description = this.get('description');
			var date = this.get('date');
			
			var newTask = this.store.createRecord('task',{
				title: title,
				description: description,
				date: new Date(date)
			});

			newTask.save();

			this.setProperties({
				title: '',
				description: '',
				date: ''
			});
			}
		},
		validateTitle: function(value) {
			this.set('errors', DS.Errors.create());
			this.validateTitle(value);
		}
	},
	validate: function() {
		this.set('errors', DS.Errors.create());
		this.validateTitle(this.get('title'));
		this.validateDate(this.get('date'));
		if(this.existing(this.get('title'))){
			this.get('errors').add('title', "Title already existed!");
		}
		return this.get('errors.isEmpty');
	},
	existing: function(title){
		let result = [];		
		this.get('model').forEach(item => {
			result.push(item.get('title'));
		});
		if(result.indexOf(title.trim()) !== -1){
			return true;
		}
		return false;
	},
	validateTitle: function(value) {
		this.get('errors').remove('title');
		if(Validate.isUndefinedOrNull(value) || Validate.isEmpty(value)){
			this.get('errors').add('title', "Can't be empty");
		}
	},
	validateDate: function(value) {
		this.get('errors').remove('date');
		if(Validate.isUndefinedOrNull(value) || Validate.isEmpty(value)){
			this.get('errors').add('date', "Can't be empty");
		}
	}
});
