import Controller from '@ember/controller';
import DS from 'ember-data';
import Validate from 'npm:validate.io';

export default Controller.extend({
	actions: {
		editTask: function(id){
			if(this.validate()){
			var self = this;
			var title = this.get('model.title');
			var description = this.get('model.description');
			var date = this.get('model.date');	
			this.store.findRecord('task', id).then(function(task){
				task.set('title', title);
				task.set('description', description);
				task.set('date', new Date(date));
				task.save();
				self.transitionToRoute('tasks');
			});
			}
		},
		validateTitle: function(value) {
			this.set('errors', DS.Errors.create());
			this.validateTitle(value);
		}
	},
	validate: function() {
		this.set('errors', DS.Errors.create());
		this.validateTitle(this.get('model.title'));
		if(this.existing(this.get('model.title'), this.get('model.id'))){
			this.get('errors').add('title', "Title already existed!");
		}
		if(Validate.isUndefinedOrNull(this.get('model.date')) || (Validate.isEmpty(this.get('model.date')) && Validate.isAbsoluteTime(this.get('model.date')))){
			this.get('errors').add('date', "Can't be empty");
		}
		return this.get('errors.isEmpty');
	},
	existing: function(title, id){
		let result = [];	
		let data = this.store.peekAll('task')
		data.forEach(item => {
			if(item.get('id') !== id){
			result.push(item.get('title'));
			}
		});
		if(result.indexOf(title.trim()) !== -1){
			return true;
		}
		return false;
	},
	validateTitle: function(value) {
		this.get('errors').remove('title');
		if(Validate.isUndefinedOrNull(value) || Validate.isEmpty(value)){
			this.get('errors').add('title', "Can't be empty");
		}
	},
});
