import Controller from '@ember/controller';
import DS from 'ember-data';
import Validate from 'npm:validate.io';
import Ember from 'ember';

export default Controller.extend({
	firebaseApp: Ember.inject.service(),
	actions: {
		submit: function(){
			if(this.validate()){
			var firstname = this.get('first_name');
			var lastname = this.get('last_name');
			var country = this.get('country');
			var email = this.get('email');
			var password = this.get('password');
			// var ref = new Firebase("https://myemberproject.firebaseio.com/");
			// var _this = this;
			// var newTask = this.store.createRecord('user',{
			// 	first_name: firstname,
			// 	last_name: lastname,
			// 	country: country,
			// 	email: email,
			// 	password: password
			// });
			// newTask.save();
			var ref = this.get('firebaseApp'); 
			var _this = this;
			ref.auth().createUserWithEmailAndPassword(
				email, 
				password
				).catch((reason) => {
					_this.get('errors').add('email', reason.message);
				}).then(function(){
				// if(error) {
				// alert(error);
				// } else {
				_this.get('session').open('firebase', {
				provider: 'password',
				'email': email,
				'password': password
				}).then(function(userData){
					var user = _this.store.createRecord('user', {
					id: userData.uid,
					first_name: firstname,
					last_name: lastname,
					country: country
					});
					user.save()
					.then(function(){
					_this.setProperties({
						first_name: '',
						last_name: '',
						country: '',
						email: '',
						password: ''
					});
					_this.transitionToRoute('protected');
					});
				});
				//}
			});
			}
		}
	},
	validate: function() {
		this.set('errors', DS.Errors.create());
		this.get('errors').remove('email');
		if(Validate.isUndefinedOrNull(this.get('email')) || Validate.isEmpty(this.get('email'))){
			this.get('errors').add('email', "Can't be empty");
		}else if(!Validate.isEmailAddress(this.get('email'))){	
			this.get('errors').add('email', "Invalid Email address");
		}
		
		if(Validate.isUndefinedOrNull(this.get('first_name')) || Validate.isEmpty(this.get('first_name'))){
			this.get('errors').add('first_name', "Can't be empty");
		}
		if(Validate.isUndefinedOrNull(this.get('last_name')) || Validate.isEmpty(this.get('last_name'))){
			this.get('errors').add('last_name', "Can't be empty");
		}
		if(Validate.isUndefinedOrNull(this.get('password')) || Validate.isEmpty(this.get('password'))){
			this.get('errors').add('password', "Can't be empty");
		}
		return this.get('errors.isEmpty');
	},
});
