import Controller from '@ember/controller';
import DS from 'ember-data';
export default Controller.extend({
	actions: {
	authenticate(provider) {
		let controller = this;
		controller.set('errorMessage', DS.Errors.create());
		var email = provider.identification;
		var password = provider.password;
		this.get('session').open('firebase', {
			provider: 'password',
			email: email,
			password: password
			}).catch(function(reason){
				controller.set('errorMessage', reason.message);
			}).then(function() {
				controller.transitionToRoute('protected');
			}.bind(this));
		}
	} 
});
