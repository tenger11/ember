import Controller from '@ember/controller';
export default Controller.extend({
	actions:{
		logout: function() {
			let controller = this;
			this.get('session').close().then(function() {
			controller.transitionToRoute('login');
			}.bind(this));
		}
	}
})